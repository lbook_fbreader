/*
 * Copyright (C) 2008 Alexander Egorov <lunohod@gmx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "ZLNXViewWidget.h"
#include "ZLNXPaintContext.h"

void ZLNXViewWidget::updateCoordinates(int &x, int &y) {
	switch (rotation()) {
		default:
			break;
		case ZLViewWidget::DEGREES90:
		{
			int tmp = x;
			x = height() - y;
			y = tmp;
			break;
		}
		case ZLViewWidget::DEGREES180:
			x = width() - x;
			y = height() - y;
			break;
		case ZLViewWidget::DEGREES270:
		{
			int tmp = x;
			x = y;
			y = width() - tmp;
			break;
		}
	}
}

int ZLNXViewWidget::width() const {
	switch (rotation()) {
		default:
			return 600;
			break;
		case ZLViewWidget::DEGREES90:
		case ZLViewWidget::DEGREES270:
			return 800;
			break;
		case ZLViewWidget::DEGREES180:
			return 600;
			break;
	}
}

int ZLNXViewWidget::height() const {
	switch (rotation()) {
		default:
			return 800;
			break;
		case ZLViewWidget::DEGREES90:
		case ZLViewWidget::DEGREES270:
			return 600;
			break;
		case ZLViewWidget::DEGREES180:
			return 800;
			break;
	}
}

ZLNXViewWidget::ZLNXViewWidget(ZLApplication *application, Angle initialAngle) : ZLViewWidget(initialAngle) {
	myApplication = application;
}

ZLNXViewWidget::~ZLNXViewWidget() {
}

void ZLNXViewWidget::repaint()	{
	ZLNXPaintContext &NXContext = (ZLNXPaintContext&)view()->context();

	NXContext.rotate((ZLNXPaintContext::Angle)rotation());
	
	//printf("repaint\n");
//	memset(buf, 0xff, 800*600/4);
	view()->paint();
}

void ZLNXViewWidget::trackStylus(bool track) {
}
